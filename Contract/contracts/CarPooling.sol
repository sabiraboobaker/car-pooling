//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.5.16;
pragma experimental ABIEncoderV2;

contract CarPooling {
    //===============================DECLARATION=============================//
    //**************************user data structure**************************//

    struct Admin {
        bytes name;
        address payable pubAdr;
        bytes32 password;
    }
    Admin public admin;

    struct User {
        bytes name;
        bytes dob;
        uint256 ph;
        bytes email;
        bytes32 password;
        mapping(uint256 => uint256) Rides;
        uint256 RideCount;
    }
    mapping(address => User) public Users;
    uint32 public UserCount;
    mapping(uint32 => address) public UserIndex;

    /**************************Path data structure **************************/
    uint8 public kmRate;
    uint8 public baseCost;
    struct Path {
        bytes place;
        uint256 dist;
        uint256 time;
        uint8 cost;
    }
    mapping(uint256 => Path) public Paths;
    uint256 public PathCount;

    /**************************Ride Data Structure *************************/
    struct Rider {
        address payable rider;
        uint256 srcIndex;
        uint256 destIndex;
        uint256 cost;
        uint256 dist;
        uint256 time;
    }

    struct Ride {
        uint256 timestamp;
        address payable driver;
        mapping(uint256 => Rider) Riders;
        uint32 srcIndex;
        uint32 destIndex;
        bytes vehicleDet;
        bytes regNo;
        uint32 availableSeat;
        uint32 occupiedSeat;
        bool status;
        uint256 amount;
    }
    mapping(uint256 => Ride) public Rides;
    uint256 public RideCount;

    //===============================FUNCTIONS=============================//
    //******************************Constructor****************************//
    constructor(bytes memory name, bytes memory password, uint8 _baseCost, uint8 _kmRate) public {
        bytes32 passHash = keccak256(password);
        admin.name = name;
        admin.pubAdr = msg.sender;
        admin.password = passHash;
        baseCost = _baseCost;
        kmRate=_kmRate;
    }

    //****************************User Registration************************//
    //regdoc
    function register(
        bytes memory name,
        bytes memory dob,
        bytes memory email,
        uint256 ph,
        bytes memory password
    ) public {
        bytes32 passHash = keccak256(password);
        Users[msg.sender].name = name;
        Users[msg.sender].dob = dob;
        Users[msg.sender].email = email;
        Users[msg.sender].ph = ph;
        Users[msg.sender].password = passHash;

        UserCount = UserCount + 1;
        UserIndex[UserCount] = msg.sender;
    }

    //****************************login***********************************//
    function login(bytes memory password)
        public
        view
        returns (
            bool,
            uint256,
            bytes memory,
            bytes memory,
            uint256
        )
    {
        bytes32 passHash = keccak256(password);
        if (admin.password == passHash && admin.pubAdr == msg.sender) {
            return (true, 1, admin.name, bytes(""), 0);
        } else if (Users[msg.sender].password == passHash) {
            return (
                true,
                2,
                Users[msg.sender].name,
                Users[msg.sender].email,
                Users[msg.sender].ph
            );
        } else {
            return (false, 0, bytes(""), bytes(""), 0);
        }
    }

    function newPath(
        uint256[] memory index,
        bytes[] memory place,
        uint8[] memory dist,
        uint256[] memory time
    ) public {
        for (uint256 i = 0; i < index.length; i++) {
            uint8 cost = dist[i] * baseCost;
            Paths[i + 1] = Path(place[i], dist[i], time[i], cost);
        }
        PathCount = index.length;
    }

    function newRide(
        uint256 timestamp,
        uint32 srcIndex,
        uint32 destIndex,
        bytes memory vehicleDet,
        bytes memory regNo,
        uint32 availableSeat
    ) public {
        RideCount = RideCount + 1;
        Rides[RideCount].timestamp = timestamp;
        Rides[RideCount].driver = msg.sender;
        Rides[RideCount].srcIndex = srcIndex;
        Rides[RideCount].destIndex = destIndex;
        Rides[RideCount].vehicleDet = vehicleDet;
        Rides[RideCount].regNo = regNo;
        Rides[RideCount].availableSeat = availableSeat;
        Rides[RideCount].status = false;
        Users[msg.sender].RideCount = Users[msg.sender].RideCount + 1;
        Users[msg.sender].Rides[Users[msg.sender].RideCount] = RideCount;
    }

    function joinRide(
        uint256 rideId,
        uint256 srcIndex,
        uint256 destIndex,
        uint256 cost,
        uint256 time,
        uint256 dist
    ) public payable {
        Rides[rideId].occupiedSeat = Rides[rideId].occupiedSeat + 1;
        Rides[rideId].amount = Rides[rideId].amount + cost;
        Rides[rideId].Riders[Rides[rideId].occupiedSeat] = Rider(
            msg.sender,
            srcIndex,
            destIndex,
            cost,
            dist,
            time
        );
        Users[msg.sender].RideCount = Users[msg.sender].RideCount + 1;
        Users[msg.sender].Rides[Users[msg.sender].RideCount] = rideId;
        
    }

    function fairCalculation(uint256 rideId) internal {
        uint256 count = Rides[rideId].occupiedSeat;
        uint256 src = Rides[rideId].srcIndex;
        uint256 dest = Rides[rideId].destIndex;
        uint8[10] memory userCount=[0,0,0,0,0,0,0,0,0,0];
        uint8[10] memory totalFair=[0,0,0,0,0,0,0,0,0,0];
        uint256 driverFair =0;

        for ( uint256 i = 1; i <= count; i++) {
            uint256 uSrc = Rides[rideId].Riders[count].srcIndex;
            uint256 uDest = Rides[rideId].Riders[count].destIndex;
            if (src < dest) {
                for (uint256 j = uSrc+1; j <= uDest; j++) {
                    uint8 fair = Paths[j].cost;
                    userCount[j] = userCount[j] +1;
                    totalFair[j] = fair;
                }
            } else if (src > dest) {
                for (uint256 j = uSrc; j > uDest; j--) {
                    uint8 fair = Paths[j].cost;
                    userCount[j] = userCount[j] +1;
                    totalFair[j] = fair;
                }
            }
        }

        for (uint256 i = 1; i <= count; i++) {
            uint256 uSrc = Rides[rideId].Riders[count].srcIndex;
            uint256 uDest = Rides[rideId].Riders[count].destIndex;
            uint256 finalFair = baseCost;
            uint256 balenceAmount =0;
            if (src < dest) {
                for (uint256 j = uSrc+1; j <= uDest; j++) {
                    finalFair=finalFair+(totalFair[j]/(userCount[j] +1));
                }
            } else if (src > dest) {
                for (uint256 j = uSrc; j > uDest; j--) {
                    finalFair=finalFair+(totalFair[j]/(userCount[j] +1));
                }
            }
            balenceAmount = Rides[rideId].Riders[count].cost - finalFair;
            driverFair = driverFair + finalFair;
            address payable rider = Rides[rideId].Riders[count].rider;
            rider.transfer(balenceAmount * 1000000000000000000);
        }
        address payable driver = Rides[rideId].driver;
        driver.transfer(driverFair * 1000000000000000000);
    }

    function rideCompleted(uint256 rideId) public {
        fairCalculation(rideId);
        Rides[rideId].status = true;
    }

    function getUserRideIndex(uint256 index) public view returns (uint256,bool) {
        bool status = false;
        uint256 rideId = Users[msg.sender].Rides[index];
        if(msg.sender == Rides[rideId].driver){
            status = true;
        }
        return (rideId,status);
    }
}
