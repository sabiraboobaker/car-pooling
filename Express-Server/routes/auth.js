var express = require('express');
var router = express.Router();

router.post('/login', async function (req, res, next) {
    let data = req.body;
    console.log(data)
  
    let publicAddress = data.publicAddress;
    let password = web3.utils.asciiToHex(data.password);
    console.log(password)
    
    CarPooling.methods.login(password)
      .call({ from: publicAddress})
      .then((val) => {
        console.log("heyyy",val);
        if (val[0] == true) {
          res.status(200).json({ status: val[0],type:val[1],name:val[2]!=null?web3.utils.hexToAscii(val[2]):"",email:val[3]!=null?web3.utils.hexToAscii(val[3]):"", ph:val[4], publicAddress:publicAddress });
        } else {
          res.status(400).json({ status: false });
        }
      })
});

router.post('/register', async function (req, res, next) {
    data = req.body;
    let name = web3.utils.asciiToHex(data.name);
    let email = web3.utils.asciiToHex(data.email);
    let ph = data.ph;
    let dob = web3.utils.asciiToHex(data.dob);
    
    let publicAddress = data.publicAddress;
    let password = web3.utils.asciiToHex(data.password);
    console.log("register called")

    var resp = await CarPooling.methods.register(
        name,
        dob,
        email,
        ph,
        password
    ).send({ from: publicAddress,gas:6000000 });

    console.log(resp);

    res.status(200).json({ status: true });
   
});

module.exports = router;
