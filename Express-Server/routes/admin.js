var express = require('express');
var router = express.Router();

router.post('/profile', function (req, res, next) {
    CarPooling.methods.admin()
        .call()
        .then((val) => {
            console.log("heyyy", val);
            var payload = {
                name: web3.utils.hexToAscii(val[0]),
                publicAddress: val[1],
            }
            res.status(200).json(payload);
        })
});

router.post('/user-list', async function (req, res, next) {
    var resp = await CarPooling.methods.UserCount().call()
    var payload = [];
    for (var i = 1; i <= resp; i++) {
        var resp2 = await CarPooling.methods.UserIndex(i).call()
        var resp3 = await CarPooling.methods.Users(resp2).call()
        payload.push({ name: web3.utils.hexToAscii(resp3["name"]), dob: web3.utils.hexToAscii(resp3["dob"]), ph: resp3["ph"], email: web3.utils.hexToAscii(resp3["email"]), rides: resp3["RideCount"] })
    }
    res.status(200).json(payload);
});


router.post('/new-path', async function (req, res, next) {
    var data = req.body;
    let publicAddress = data.publicAddress;
    console.log(data);
    var index = [];
    var place = [];
    var dist = [];
    var time = [];

    data["path"].forEach(element => {
        index.push(element["index"]);
        place.push(web3.utils.asciiToHex(element["place"]));
        dist.push(element["dist"]);
        time.push(element["time"]);
    });

    console.log(index, place, time, dist)

    var resp = await CarPooling.methods.newPath(
        index,
        place,
        dist,
        time
    ).send({ from: publicAddress, gas: 6000000 });
    console.log(resp);

    res.status(200).json({ status: true });
});

router.post('/get-path', async function (req, res, next) {
    var payload = []
    var resp = await CarPooling.methods.PathCount().call()
    console.log(resp)
    for (var i = 1; i <= resp; i++) {
        var resp1 = await CarPooling.methods.Paths(i).call()
        console.log(resp1)
        payload.push({ index: i, place: web3.utils.hexToAscii(resp1[0]), dist: resp1[1], time: resp1[2], cost: resp1[3] })

    }


    res.status(200).json(payload);

});


router.post('/list-rides', async function (req, res, next) {
    var payload = []
    var resp = await CarPooling.methods.RideCount().call()
    console.log(resp)
    for (var i = 1; i <= resp; i++) {
        var resp1 = await CarPooling.methods.Rides(i).call()
        console.log(resp1)
        var resp2 = await CarPooling.methods.Users(resp1[1]).call()
        var resp3 = await CarPooling.methods.Paths(resp1[2]).call()
        var resp4 = await CarPooling.methods.Paths(resp1[3]).call()

        payload.push(
            {
                timestamp:resp1[0],
                srcIndex:web3.utils.hexToAscii(resp3[0]),
                destIndex:web3.utils.hexToAscii(resp4[0]),
                vehicleDet: web3.utils.hexToAscii(resp1[4]),
                regNo: web3.utils.hexToAscii(resp1[5]),
                availableSeat:resp1[6],
                occupiedSeat:resp1[7],
                status:resp1[8],
                driver:web3.utils.hexToAscii(resp2[0])
            }
        )

    }


    res.status(200).json(payload);

});

module.exports = router;
