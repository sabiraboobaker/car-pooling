var express = require('express');
var router = express.Router();

router.post('/profile', function (req, res, next) {
  data = req.body;
  let publicAddress = data.publicAddress;
  CarPooling.methods.Users(publicAddress)
    .call()
    .then((val) => {
      console.log("heyyy", val);
      var payload = {
        name: web3.utils.hexToAscii(val[0]),
        publicAddress: publicAddress,
        email: web3.utils.hexToAscii(val[3]),
        ph: val[2],
        dob: web3.utils.hexToAscii(val[1]),
      }
      res.status(200).json(payload);
    })
});


router.post('/new-ride', async function (req, res, next) {
  data = req.body;
  let publicAddress = data.publicAddress;
  let timestamp = data.timestamp;
  let srcIndex = data.srcIndex;
  let destIndex = data.destIndex;
  let availableSeat = data.availableSeat;
  let vehicleDet = web3.utils.asciiToHex(data.vehicleDet);
  let regNo = web3.utils.asciiToHex(data.regNo);

  var resp = await CarPooling.methods.newRide(
    timestamp,
    srcIndex,
    destIndex,
    vehicleDet,
    regNo,
    availableSeat
  ).send({ from: publicAddress, gas: 6000000 });

  console.log(resp);

  res.status(200).json({ status: true });



});


router.post('/list-rides', async function (req, res, next) {
  data = req.body;
  let srcIndex = data.srcIndex;
  let destIndex = data.destIndex;


  var cost = 0;
  var dist = 0;
  var time = 0;
  var result = []


  var resp = await CarPooling.methods.RideCount().call()
  console.log(resp)
  for (var i = 1; i <= resp; i++) {
    var resp1 = await CarPooling.methods.Rides(i).call()
    console.log(resp1)
    //status & timestamp & availablility
    // index direction
    if (resp1[8] == false && resp1[6] > resp1[7]) {

      if (srcIndex > destIndex) {

        if (srcIndex <= resp1[2] && destIndex >= resp1[3]) {
          var resp2 = await CarPooling.methods.Users(resp1[1]).call()
          var resp5 = await CarPooling.methods.Paths(resp1[2]).call()
          var resp6 = await CarPooling.methods.Paths(resp1[3]).call()
          result.push(
            {
              rideID:i,
              timestamp: resp1[0],
              srcIndex: web3.utils.hexToAscii(resp5[0]),
              destIndex: web3.utils.hexToAscii(resp6[0]),
              vehicleDet: web3.utils.hexToAscii(resp1[4]),
              regNo: web3.utils.hexToAscii(resp1[5]),
              availableSeat: resp1[6],
              occupiedSeat: resp1[7],
              status: resp1[8],
              driver: web3.utils.hexToAscii(resp2[0])
            }
          )
        }

      } else if (srcIndex < destIndex) {
        if (srcIndex >= resp1[2] && destIndex <= resp1[3]) {
          var resp2 = await CarPooling.methods.Users(resp1[1]).call()
          var resp5 = await CarPooling.methods.Paths(resp1[2]).call()
          var resp6 = await CarPooling.methods.Paths(resp1[3]).call()
          result.push(
            {
              rideID:i,
              timestamp: resp1[0],
              srcIndex: web3.utils.hexToAscii(resp5[0]),
              destIndex: web3.utils.hexToAscii(resp6[0]),
              vehicleDet: web3.utils.hexToAscii(resp1[4]),
              regNo: web3.utils.hexToAscii(resp1[5]),
              availableSeat: resp1[6],
              occupiedSeat: resp1[7],
              status: resp1[8],
              driver: web3.utils.hexToAscii(resp2[0])
            }
          )
        }
      }
    }
  }
  if (srcIndex > destIndex) {

    for (i = srcIndex; i > destIndex; i--) {
      var resp3 = await CarPooling.methods.Paths(i).call()
      cost = cost + Number(resp3[3]);
      time = time + Number(resp3[2]);
      dist = dist + Number(resp3[1]);
    }
  } else if (srcIndex < destIndex) {
    for (i = destIndex; i > srcIndex; i--) {
      var resp3 = await CarPooling.methods.Paths(i).call()
      cost = cost + Number(resp3[3]);
      time = time + Number(resp3[2]);
      dist = dist + Number(resp3[1]);
    }
  }

  var payload = { result, time, cost, dist }


  res.status(200).json(payload);

});


router.post('/join-ride', async function (req, res, next) {
  data = req.body;
  let publicAddress = data.publicAddress;
  let rideId = data.rideId;
  let srcIndex = data.srcIndex;
  let destIndex = data.destIndex;
  let time = data.time;
  let cost = data.cost;
  let dist = data.dist;

  console.log(data)

  var resp = await CarPooling.methods.joinRide(
    rideId,
    srcIndex,
    destIndex,
    cost,
    time,
    dist,
  ).send({ from: publicAddress, gas: 6000000, value: web3.utils.toWei(String(cost), 'ether') });

  console.log(resp);

  res.status(200).json({ status: true });
});

router.post('/complete-ride', async function (req, res, next) {
  data = req.body;
  
  let rideId = data.rideId;
  let publicAddress = data.publicAddress
 
  console.log(data)
  
  var resp = await CarPooling.methods.rideCompleted(
    rideId
  ).send({ from: publicAddress, gas: 6000000});

  console.log(resp);

  res.status(200).json({ status: true });
});

router.post('/my-rides', async function (req, res, next) {
  data = req.body;
  let publicAddress = data.publicAddress;
  var resp = await CarPooling.methods.Users(publicAddress).call()
  console.log(resp)
  var payload = [];
  for (var i = 1; i <= resp["RideCount"]; i++) {
    var resp2 = await CarPooling.methods.getUserRideIndex(i).call({ from: publicAddress })
    var resp3 = await CarPooling.methods.Rides(resp2[0]).call()
    var resp4 = await CarPooling.methods.Users(resp3[1]).call()
    var resp5 = await CarPooling.methods.Paths(resp3[2]).call()
    var resp6 = await CarPooling.methods.Paths(resp3[3]).call()
    console.log(resp3);
    payload.push(
      {
        rideID:resp2[0],
        timestamp: resp3[0],
        driver: web3.utils.hexToAscii(resp4[0]),
        isDriver:resp2[1],
        srcIndex: web3.utils.hexToAscii(resp5[0]),
        destIndex: web3.utils.hexToAscii(resp6[0]),
        vehicleDet: web3.utils.hexToAscii(resp3[4]),
        regNo: web3.utils.hexToAscii(resp3[5]),
        availableSeat: resp3[6],
        occupiedSeat: resp3[7],
        status: resp3[8],
        amount: resp3[9]

      }
    )
  }
  res.status(200).json(payload);
});


module.exports = router;
