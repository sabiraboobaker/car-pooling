var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

/*-------------------WEB3 Connection Starts---------------------*/
const Web3 = require('web3');
var CarPoolingJSON = require(path.join(__dirname, './../Contract/build/contracts/CarPooling.json'));

//for ganache
web3 = new Web3('ws://localhost:7545');
CarPoolingAddress = CarPoolingJSON.networks['5777'].address;

const CarPoolingAbi = CarPoolingJSON.abi;

CarPooling = new web3.eth.Contract(CarPoolingAbi, CarPoolingAddress);
/*-------------------WEB3 Connection Ends----------------------*/

var auth = require('./routes/auth');
var admin = require('./routes/admin');
var users = require('./routes/users');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser())
app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers","Cache-control,Pragma,Origin,Authorization,Content-Type,X-Requested-With");
    res.header("Access-Control-Allow-Methods","GET,PUT,POST");
    if('OPTIONS'===req.method){
        res.status(204).send();
    }
    else{
        next();
    }
});

app.use('/', auth);
app.use('/admin', admin);
app.use('/user', users);

module.exports = app;
