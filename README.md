# Car Pooling


## Introduction
Car Pooling project in Ethereum Blockchain. This project consist of 3 sections.

### Web 
This folder consist of Front-end website in Angular.

### Express-Server
This folder consist of the Back-end code which intracts with the contract and Front-end.

### Contract
This  folder consist of truffle framework which compile, deploy and test smart contract in ethereum blockchain.



## Pre-Requisite
* git
* Code Editor
* Browser
* Ganache
* Nodejs
* Truffle Framework


## Initial Setup
1. Clone the project.
    ```bash
        git clone https://gitlab.com/sabiraboobaker/car-pooling.git
    ```
2. Install Dependencies in both `Express-Server` and `Web` folders.
    ```bash
    cd Web
    npm i
    cd ..
    cd Express-Server
    npm i
    ```

## Run Project
### Deploy Contract
1. Open Ganache and run emulator.
2. Make sure the ip and port number in `Contract/truffle-config.js` in  folder is same as specified in ganache.
3. Open terminal in `Contract` folder and Run following command to deploy contract.
    ```bash
    truffle migrate --reset
    ```
4. Make sure you dont close ganache while running the project.

### Run Express Server
1. Open  `Express-Server/App.js` and make sure the ip and port specified is same as specified in ganache.
2. Open terminal in `Express-Server` folder and Run following command to run server.
    ```bash
    npm start
    ```

### Run React Web
1. Open terminal in `Web` folder and Run following command to run server.
    ```bash
    ng serve
    ```
2. open http://localhost:4200 in browser and start using the application.

