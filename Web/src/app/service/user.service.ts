import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  getUserProfile(data:any): Observable<any> {
    let url = this.baseUrl+'/user/profile';
    return this.http.post<any>(`${url}`,{publicAddress:data});
  }
  getAdminProfile(data:any): Observable<any> {
    let url = this.baseUrl+'/admin/profile';
    return this.http.post<any>(`${url}`,{publicAddress:data});
  }
  getAll(){
    let url = this.baseUrl+'/admin/user-list';
    return this.http.post<any>(`${url}`, "");
  }

}
