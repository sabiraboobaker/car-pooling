import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from './user.mode';

export interface AuthResponseData {
  id: string;
  name: string;
  email: string;
  phone: string;
  dob: string;
  publicAddress: string;
  refreshTocken: string;
  expiresIn?: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user = new Subject<User>();
  // user = new BehaviorSubject<User>(null);

  authBaseUrl = environment.baseUrl;
  constructor(private http: HttpClient, private router: Router) { }

  login(publicAddress: string, password: string) {
    let data = {
      publicAddress: publicAddress,
      password: password
    };
    return this.http
      .post<any>(this.authBaseUrl + "/login", data)
      .pipe(
        tap(resData => {
          let data = resData;
          console.log("login data", data);
          if (data.type == "1") {
            this.router.navigate(['/admin/profile']);
          }
          else {
            this.router.navigate(['/user/profile']);

          }

          this.handleAuthentication(data.id, data.name, data.email, data.ph, data.dob, data.publicAddress)
        })
      );
  }

  register(name: string, ph: string, email: string, dob: string, password: string, publicAddress: string) {
    let data = {
      name: name,
      ph: ph,
      dob: dob,
      email: email,
      password: password,
      publicAddress: publicAddress
    };
    // console.log("------------------------------------------");

    return this.http
      .post<any>(this.authBaseUrl + "/register", data)
      .pipe(
        tap(resData => {
          alert("Registered")
          window.location.href = "/"
          
        })
      );
  }



  private handleAuthentication(id: string, name: string, email: string, ph: string, dob: string, publicAddress: string,) {
    const user = new User(id, name, email, ph, dob, publicAddress);
    this.user.next(user);
    //Write Auto logout codes here
    localStorage.setItem('userData', JSON.stringify(user));
  }






}
