import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RideService {

  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }
  
  getAll(){
    let url = this.baseUrl+'/admin/list-rides';
    return this.http.post<any>(`${url}`,"");
  }

  getMyRides(data:any){
    let url = this.baseUrl+'/user/my-rides';
    return this.http.post<any>(`${url}`,data);
  }

  searchRides(data:any){
    let url = this.baseUrl+'/user/list-rides';
    return this.http.post<any>(`${url}`,data);
  }

  newRide(data: any) {
    let url = this.baseUrl+'/user/new-ride';
    return this.http.post<any>(`${url}`,data);
  }

  JoinRide(data: any) {
    let url = this.baseUrl+'/user/join-ride';
    return this.http.post<any>(`${url}`,data);
  }

  CompleteRide(data: any) {
    let url = this.baseUrl+'/user/complete-ride';
    return this.http.post<any>(`${url}`,data);
  }
}
