import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RoutesService {

  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }
  
  get(){
    let url = this.baseUrl+'/admin/get-path';
    return this.http.post<any>(`${url}`,"");
  }

  add(data: any) {
    let url = this.baseUrl+'/admin/new-path';
    let user = JSON.parse(localStorage.getItem('userData')|| '{}');

    return this.http.post<any>(`${url}`,{publicAddress:user.publicAddress,path:data
      });
  }}
