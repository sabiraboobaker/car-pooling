import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/service/auth.service';
import {  User } from 'src/app/service/user.mode';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService,private router: Router) { }
  loading:boolean = false;
  hasError: boolean = false;

  ngOnInit(): void {
  }
  

  onSubmit(loginForm : NgForm){
    Object.keys(loginForm.controls).forEach(key => {
      loginForm.controls[key].markAsTouched();
    });
    console.log(loginForm);
    
    if (!loginForm.valid) {
      return;
    }
    // this.authService.
    this.loading = true;
    console.log("SUBMITTED");
    
    // const email = "admin";
    // const password = "Admin@2020";

    const publicAddress = loginForm.value.publicAddress;
    const password = loginForm.value.password;

    let authObs: Observable<User>

    this.authService.login(publicAddress,password).subscribe(
      res => {
        
        this.loading = false;       
      },error => {
        this.hasError = true;
        this.loading = false;
      }
    );
  }

}
