import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  constructor(private userService:UserService) { }

  data:any;
  user={
    name:"",
    publicAddress:"",
    dob:"",
    ph:"",
    email:""
  };

  ngOnInit(): void {
    this.data = JSON.parse(localStorage.getItem('userData')|| '{}');
    if(this.data != null){     
      console.log("userData",this.data.publicAddress)
      this.userProfile(this.data.publicAddress)
    }
  }



  userProfile(data: any){
    console.log("==========",data);
    
    this.userService.getUserProfile(data).subscribe(res => {
      console.log("********",res);
      
     this.user = res;
     console.log("--------",this.user);
     
   })
  }


}
