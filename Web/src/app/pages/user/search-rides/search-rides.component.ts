import { Component, OnInit } from '@angular/core';
import { RoutesService } from 'src/app/service/routes.service';
import { RideService } from 'src/app/service/ride.service';

@Component({
  selector: 'app-search-rides',
  templateUrl: './search-rides.component.html',
  styleUrls: ['./search-rides.component.css']
})
export class SearchRidesComponent implements OnInit {
  roadMap: any[] = []
  publicAddress:string="";
  srcIndex: number = 0;
  destIndex: number = 0;
  rides:any;
  cost:number=0;
  dist:number=0;
  time:number=0;

  constructor(private routesService: RoutesService,private rideService: RideService) { }

  ngOnInit(): void {
    this.routesService.get().subscribe(res => {
      console.log("********", res);

      this.roadMap = res;

    })

    var data = JSON.parse(localStorage.getItem('userData') || '{}');
    if (data != null) {
      console.log("userData", data.publicAddress)
      this.publicAddress = data.publicAddress
    }
  }

  onSearch(){
    console.log("hello")
    var payload = {
      "srcIndex": this.srcIndex,
      "destIndex": this.destIndex,
    }
    console.log(payload)
    this.rideService.searchRides(payload).subscribe(res=>{
      console.log(res)
      this.rides=res["result"]
      this.cost=res["cost"]
      this.time=res["time"]
      this.dist=res["dist"]
    })
  }

  bookNow(id:number){
    console.log(id)
    var payload={
      publicAddress:this.publicAddress,
      rideId:id,
      srcIndex:this.srcIndex,
destIndex:this.destIndex,
time:this.time,
cost:this.cost,
dist:this.dist,

    }

    this.rideService.JoinRide(payload).subscribe(res=>{
      console.log(res)
      alert("Success")
    })
  }

}
