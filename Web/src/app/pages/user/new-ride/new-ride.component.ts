import { Component, OnInit } from '@angular/core';
import { RideService } from 'src/app/service/ride.service';
import { RoutesService } from 'src/app/service/routes.service';


@Component({
  selector: 'app-new-ride',
  templateUrl: './new-ride.component.html',
  styleUrls: ['./new-ride.component.css']
})
export class NewRideComponent implements OnInit {
  roadMap: any[] = []
  publicAddress: string = ""
  timestamp: number = Date.now()
  srcIndex: number = 0;
  destIndex: number = 0;
  vehicleDet: string = "";
  regNo: string = "";
  availableSeats: number = 0;


  constructor(private rideService: RideService, private routesService: RoutesService) { }


  ngOnInit(): void {
    var data = JSON.parse(localStorage.getItem('userData') || '{}');
    if (data != null) {
      console.log("userData", data.publicAddress)
      this.publicAddress = data.publicAddress
    }
    this.routesService.get().subscribe(res => {
      console.log("********", res);

      this.roadMap = res;

    })

  }

  submit() {

    var payload = {
      "publicAddress": this.publicAddress,
      "timestamp": this.timestamp,
      "srcIndex": this.srcIndex,
      "destIndex": this.destIndex,
      "vehicleDet": this.vehicleDet,
      "regNo": this.regNo,
      "availableSeat": this.availableSeats
    }
    console.log(payload)
    this.rideService.newRide(payload).subscribe(res=>{
      console.log(res)
      alert("success")
      window.location.reload()
    })
  }

}
