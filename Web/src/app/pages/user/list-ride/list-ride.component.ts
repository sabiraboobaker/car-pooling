import { Component, OnInit } from '@angular/core';
import { RideService } from 'src/app/service/ride.service'

@Component({
  selector: 'app-list-ride',
  templateUrl: './list-ride.component.html',
  styleUrls: ['./list-ride.component.css']
})
export class ListRideComponent implements OnInit {

  rides: any;
  constructor(private rideService: RideService) { }


  ngOnInit(): void {
    this.getAllRides()
  }

  getAllRides() {
    var data = JSON.parse(localStorage.getItem('userData') || '{}');
    if (data != null) {
      console.log("userData", data.publicAddress)
      var payload = {
        publicAddress: data.publicAddress
      }

      this.rideService.getMyRides(payload).subscribe(res => {
        console.log("********", res);

        this.rides = res;
        console.log("--------", this.rides);

      })
    }

  }

  changeStatus(rideId: number) {
    var data = JSON.parse(localStorage.getItem('userData') || '{}');
    if (data != null) {
      console.log("userData", data.publicAddress)
      var payload = {
        rideId: rideId,
        publicAddress: data.publicAddress
      }
      this.rideService.CompleteRide(payload).subscribe(res => {
        console.log("********", res);

        alert("success")
        this.getAllRides()
      })
    }
  }

  toDate(dt:any){
    console.log(dt)
    var dt1=String(dt)
    var s = new Date(dt1)
    return s;
  }

}
