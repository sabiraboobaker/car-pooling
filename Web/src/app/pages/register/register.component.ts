import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/service/auth.service';
import {  User } from 'src/app/service/user.mode';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private authService: AuthService,private router: Router) { }
  loading:boolean = false;
  hasError: boolean = false;

  ngOnInit(): void {
   
  }
  onSubmit(registerForm : NgForm){
    console.log("-------------------",registerForm);
    Object.keys(registerForm.controls).forEach(key => {
      registerForm.controls[key].markAsTouched();
    });

    if (!registerForm.valid) {
      return;
    }
    // this.authService.
    this.loading = true;
    console.log("SUBMITTED");
    
    // const email = "admin";
    // const password = "Admin@2020";
    const email = registerForm.value.email ;
    const password = registerForm.value.password;
    const ph = registerForm.value.ph;
    const name = registerForm.value.name;
    const dob = registerForm.value.dob;

    const publicAddress = registerForm.value.publicAddress;

    let authObs: Observable<User>

    this.authService.register(name,ph,email,dob,password,publicAddress).subscribe(
      res => {
        this.loading = false;
        console.log("success");
        
        this.router.navigate(['/login']);
      },error => {
        console.log("error",error);
        this.hasError = true;
        this.loading = false;
      }
    );
  }

}
