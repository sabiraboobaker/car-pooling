import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/service/user.service';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  user: any;

  constructor(private userService:UserService) { }

  ngOnInit(): void {
    this.getAllUser()
  }

  getAllUser(){
    this.userService.getAll().subscribe(res => {
      console.log("********",res);
      
     this.user = res;
     console.log("--------",this.user);
     
   })
  
  }

}
