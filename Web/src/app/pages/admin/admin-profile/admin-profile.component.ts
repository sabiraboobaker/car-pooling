import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-admin-profile',
  templateUrl: './admin-profile.component.html',
  styleUrls: ['./admin-profile.component.css']
})
export class AdminProfileComponent implements OnInit {

  constructor(private userService:UserService) { }

  data:any;
  user={
    name:"",
    publicAddress:""
  };

  ngOnInit(): void {
    this.data = JSON.parse(localStorage.getItem('userData')|| '{}');
    if(this.data != null){     
      console.log("userData",this.data.publicAddress)
      this.userProfile(this.data.publicAddress)
    }
  }



  userProfile(data: any){
    console.log("==========",data);
    
    this.userService.getAdminProfile(data).subscribe(res => {
      console.log("********",res);
      
     this.user = res;
     console.log("--------",this.user);
     
   })
  }

}
