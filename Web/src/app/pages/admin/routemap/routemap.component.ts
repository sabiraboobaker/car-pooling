import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RoutesService } from 'src/app/service/routes.service';

@Component({
  selector: 'app-routemap',
  templateUrl: './routemap.component.html',
  styleUrls: ['./routemap.component.css']
})
export class RoutemapComponent implements OnInit {

  routes: any;

  constructor(private routesService: RoutesService, private router: Router) { }

  data: any;
  roadMap: any[] = [ ]
  keyCount: number = 0

  ngOnInit(): void {
    this.routesService.get().subscribe(res => {
      console.log("********", res);

      this.roadMap = res;

    })

  }

  onSubmit() {


    this.keyCount = this.roadMap.length;
    for (var i = 0; i < this.keyCount; i++) {
      this.roadMap[i]["index"] = i + 1;
    }



    

      this.routesService.add(this.roadMap).subscribe(
        res => {

          console.log("success", res);

          alert("Saved")
        })

    

  }

  addLocation(): void {
    // this.keyCount++;
    this.roadMap.push({
      place: "",
      time: 0,
      dist: 0,
    })
  }

  removeLocation(index: number): void {
    this.roadMap.splice(index, 1)
  }

  changeValue(index: number, attribute: string, event: Event): void {
    const target = event.target as HTMLInputElement

    this.roadMap[index][attribute] = target.value;
  }



}
