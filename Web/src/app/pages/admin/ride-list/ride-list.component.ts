import { Component, OnInit } from '@angular/core';
import { RideService } from 'src/app/service/ride.service'
@Component({
  selector: 'app-ride-list',
  templateUrl: './ride-list.component.html',
  styleUrls: ['./ride-list.component.css']
})
export class RideListComponent implements OnInit {
rides:any;
  constructor(private rideService:RideService) { }


  ngOnInit(): void {
    this.getAllRides()
  }

  getAllRides(){
    this.rideService.getAll().subscribe(res => {
      console.log("********",res);
      
     this.rides = res;
     console.log("--------",this.rides);
     
   })
  
  }

}
