import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';

import { AdminProfileComponent } from './pages/admin/admin-profile/admin-profile.component';
import { UserListComponent } from './pages/admin/user-list/user-list.component';
import { RoutemapComponent } from './pages/admin/routemap/routemap.component';
import { RideListComponent } from './pages/admin/ride-list/ride-list.component';

import { UserProfileComponent } from './pages/user/user-profile/user-profile.component';
import { NewRideComponent } from './pages/user/new-ride/new-ride.component'
import { ListRideComponent } from './pages/user/list-ride/list-ride.component'
import { SearchRidesComponent } from './pages/user/search-rides/search-rides.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: LoginComponent },
      { path: 'register', component: RegisterComponent },

      { path: 'admin/profile', component: AdminProfileComponent },
      { path: 'admin/user-list', component: UserListComponent },
      { path: 'admin/routes', component: RoutemapComponent },
      { path: 'admin/ride-list', component: RideListComponent },

      { path: 'user/profile', component: UserProfileComponent },
      { path: 'user/new-ride', component: NewRideComponent },
      { path: 'user/my-rides', component: ListRideComponent },
      { path: 'user/find-ride', component: SearchRidesComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
