import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
// import '@angular/compiler';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { CommonHeaderComponent } from './components/header/common-header/common-header.component';
import { AdminHeaderComponent } from './components/header/admin-header/admin-header.component';
import { UserHeaderComponent } from './components/header/user-header/user-header.component';

import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';

import { AdminProfileComponent } from './pages/admin/admin-profile/admin-profile.component';
import { UserListComponent} from './pages/admin/user-list/user-list.component'
import { RoutemapComponent } from './pages/admin/routemap/routemap.component';
import { RideListComponent } from './pages/admin/ride-list/ride-list.component';

import { SearchRidesComponent } from './pages/user/search-rides/search-rides.component';
import { UserProfileComponent } from './pages/user/user-profile/user-profile.component';
import { NewRideComponent } from './pages/user/new-ride/new-ride.component';
import { ListRideComponent } from './pages/user/list-ride/list-ride.component';

@NgModule({
  declarations: [
    AppComponent,

    CommonHeaderComponent,
    AdminHeaderComponent,
    UserHeaderComponent,

    LoginComponent,
    RegisterComponent,
    
    AdminProfileComponent,
    UserListComponent,
    RoutemapComponent,
    RideListComponent,
    SearchRidesComponent,
    UserProfileComponent,
    NewRideComponent,
    ListRideComponent,


    // ProfileComponent,
    // RoutesComponent,
    // RoutesOperationComponent,
    // UserListComponent,
    // HeaderComponent,
    // CreateRideComponent,
    // PastRideComponent,
    // ActiveRideComponent,
    // ListRideComponent,
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
